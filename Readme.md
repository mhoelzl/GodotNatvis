Debugger Visualizations for Godot
=================================

This project consists of custom debugger visualizations for Godot data structures
that improve the debugging experience in Visual Studio.

To make use of the project, just copy the `*.natvis` file(s) from this project
into the root of your Godot source tree.